class CreateRents < ActiveRecord::Migration
  def change
    create_table :rents do |t|
      t.references :user, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true
      t.references :room, index: true, foreign_key: true
      t.datetime :start_at
      t.datetime :end_at
      t.decimal :tmp_price, precision: 10, scale: 2
      t.decimal :total_price, precision: 10, scale: 2
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
