class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :number, limit: 20
      t.string :type, limit: 20
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
