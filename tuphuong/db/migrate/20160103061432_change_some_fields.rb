class ChangeSomeFields < ActiveRecord::Migration
  def change
  	rename_column :prices, :price, :price_value
  	change_column :prices, :time, :text
  end
end
