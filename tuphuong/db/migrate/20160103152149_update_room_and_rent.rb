class UpdateRoomAndRent < ActiveRecord::Migration
  def change
  	add_column :rooms, :active_rent_id, :number
  	remove_foreign_key :rents, column: :user_id
  	remove_foreign_key :rents, column: :client_id
  	add_column :prices, :price_id, :number, index: true, foreign_key: true
  end
end
