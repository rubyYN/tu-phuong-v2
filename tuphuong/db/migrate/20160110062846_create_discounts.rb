class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.string :name
      t.string :code
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps null: false
    end
  end
end
