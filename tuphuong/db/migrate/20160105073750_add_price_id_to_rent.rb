class AddPriceIdToRent < ActiveRecord::Migration
  def change
    add_column :rents, :price_id, :number
  end
end
