class AddTotalHoursToRents < ActiveRecord::Migration
  def change
    add_column :rents, :total_hours, :number
  end
end
