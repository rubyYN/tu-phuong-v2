class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.datetime :time
      t.string :type, limit: 20
      t.decimal :price, precision: 10, scale: 2
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
