class AddRentTypeToRent < ActiveRecord::Migration
  def change
    add_column :rents, :rent_type, :string
  end
end
