class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name, limit: 100
      t.string :phone, limit: 100
      t.string :address, limit: 100
      t.string :p_id, limit: 100
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
