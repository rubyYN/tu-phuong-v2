class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, limit: 50
      t.string :username, limit: 20
      t.string :fullname, limit: 100
      t.references :role, index: true, foreign_key: true
      t.datetime :last_login_at
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
