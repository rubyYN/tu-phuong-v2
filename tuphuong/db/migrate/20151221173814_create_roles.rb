class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name, limit: 20
      t.string :description, limit: 100
      t.integer :count, limit: 10
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
