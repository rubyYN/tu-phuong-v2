# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160110062846) do

  create_table "clients", force: :cascade do |t|
    t.string   "name",       limit: 100
    t.string   "phone",      limit: 100
    t.string   "address",    limit: 100
    t.string   "p_id",       limit: 100
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "discounts", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prices", force: :cascade do |t|
    t.text     "time"
    t.string   "price_type",  limit: 20
    t.decimal  "price_value",            precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price_id"
  end

  create_table "rents", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "client_id"
    t.integer  "room_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.decimal  "tmp_price",   precision: 10, scale: 2
    t.decimal  "total_price", precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price_id"
    t.string   "rent_type"
    t.decimal  "total_hours"
  end

  add_index "rents", ["client_id"], name: "index_rents_on_client_id"
  add_index "rents", ["room_id"], name: "index_rents_on_room_id"
  add_index "rents", ["user_id"], name: "index_rents_on_user_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name",        limit: 20
    t.string   "description", limit: 100
    t.integer  "count",       limit: 10
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "number",         limit: 20
    t.string   "room_type",      limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.decimal  "active_rent_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",           limit: 50
    t.string   "username",        limit: 20
    t.string   "fullname",        limit: 100
    t.integer  "role_id"
    t.datetime "last_login_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
  end

  add_index "users", ["role_id"], name: "index_users_on_role_id"

end
