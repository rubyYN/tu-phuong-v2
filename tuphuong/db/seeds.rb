Discount.create!([
  {name: "tuphuong", code: "tuphuong@123456"}
])
Price.create!([
  {time: "1h", price_type: "normal", price_value: "70000.0", price_id: nil},
  {time: "1h+", price_type: "normal", price_value: "20000.0", price_id: nil},
  {time: "overnight", price_type: "normal", price_value: "180000.0", price_id: nil},
  {time: "day", price_type: "normal", price_value: "250000.0", price_id: nil},
  {time: "1h", price_type: "vip", price_value: "100000.0", price_id: nil},
  {time: "1h+", price_type: "vip", price_value: "30000.0", price_id: nil},
  {time: "overnight", price_type: "vip", price_value: "250000.0", price_id: nil},
  {time: "day", price_type: "vip", price_value: "350000.0", price_id: nil},
  {time: "1h", price_type: "other", price_value: "80000.0", price_id: nil},
  {time: "1h+", price_type: "other", price_value: "20000.0", price_id: nil},
  {time: "overnight", price_type: "other", price_value: "200000.0", price_id: nil},
  {time: "day", price_type: "other", price_value: "300000.0", price_id: nil}
])
Role.create!([
  {name: "admin", description: "Admin", count: nil},
  {name: "mod", description: "Moderator", count: nil}
])
Room.create!([
  {number: "101", room_type: "other", status: "cleaning", active_rent_id: nil},
  {number: "102", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "201", room_type: "normal", status: "empty", active_rent_id: nil},
  {number: "202", room_type: "normal", status: "empty", active_rent_id: nil},
  {number: "203", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "204", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "205", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "301", room_type: "vip", status: "cleaning", active_rent_id: nil},
  {number: "302", room_type: "vip", status: "cleaning", active_rent_id: nil},
  {number: "303", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "304", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "305", room_type: "other", status: "cleaning", active_rent_id: nil},
  {number: "306", room_type: "other", status: "cleaning", active_rent_id: nil},
  {number: "401", room_type: "vip", status: "cleaning", active_rent_id: nil},
  {number: "402", room_type: "vip", status: "cleaning", active_rent_id: nil},
  {number: "403", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "404", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "405", room_type: "other", status: "cleaning", active_rent_id: nil},
  {number: "406", room_type: "other", status: "cleaning", active_rent_id: nil},
  {number: "501", room_type: "vip", status: "cleaning", active_rent_id: nil},
  {number: "502", room_type: "normal", status: "cleaning", active_rent_id: nil},
  {number: "503", room_type: "normal", status: "cleaning", active_rent_id: nil}
])
User.create!([
  {email: "admin@tuphuonghotel.com", username: "admin", fullname: "Admin", role_id: 1, last_login_at: "2016-01-14 17:50:46", password_digest: "$2a$10$TOfNJLVbR30jj94FcCkhA.2Pi0GhPAz8CM8.n1bTHgJ3.JV3tTOHK"},
  {email: "nv1@tuphuonghotel.com", username: "nv1", fullname: "Nhân viên 1", role_id: 2, last_login_at: "2016-01-14 17:51:43", password_digest: "$2a$10$o0vetLM7IPcNMvIv8WmCj.neIt.3iXEt.u4y6kYX3ALmO4Ezf3LES"},
  {email: "nv2@tuphuonghotel.com", username: "nv2", fullname: "Nhân viên 2", role_id: 2, last_login_at: nil, password_digest: "$2a$10$o0vetLM7IPcNMvIv8WmCj.neIt.3iXEt.u4y6kYX3ALmO4Ezf3LES"}
])
