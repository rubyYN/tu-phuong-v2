class DiscountsController < ApplicationController
	def index
    	@discounts = Discount.all
  	end

  	def show
    	@discount = Discount.find(params[:id])
  	end

	def new
		@discount = Discount.new
	end
	
	def edit
  		@discount = Discount.find(params[:id])
	end

	def create
		if params[:format] == 'json'
			@discount = Discount.where(:name => params[:name],:code => params[:code]).first
			if @discount
				if @room = Room.find(params[:room_id]) and @rent = @room.rents.find(params[:rent_id])
					@rent.total_price = @rent.total_price - params[:price].to_f
					if @rent.save
						render :json => @discount, :status => 200
					else
						render :json => '11', :status => 0
					end
				else
					render :json => '12', :status => 0
				end
			else
				render :json => '23', :status => 0
			end
		else
		  	@discount = Discount.new(discount_params)
			if @discount.save
				flash[:notice] = "Tạo Discount thành công"
				redirect_to discounts_path
			else
	    		render 'new'
	    	end
	    end
	end

	def update
	  @discount = Discount.find(params[:id])

	  if params[:format] == 'json'
	  	@discount.status = params[:discount_status]
	  	if @discount.save
	  		render :json => @discount , :status => 200
	  	elsif 
	  		render :json => @discount, :status => 0
  		end		
	  elsif @discount.update(discount_params)
	  	flash[:notice] = "Sua Discount thành công"
	    redirect_to discounts_path
	  else
	    render 'edit'
	  end
	end

	def destroy
		@discount = Discount.find(params[:id])
	  	@discount.destroy
	 
	  	redirect_to discounts_path
	end

	private
	  	def discount_params
	   		params.require(:discount).permit(:name, :code)
	  	end
end
