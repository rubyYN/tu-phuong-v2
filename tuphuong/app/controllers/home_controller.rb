class HomeController < ApplicationController
  def index
  	@rooms = Room.order('number ASC').all
  	#can use : @rooms = Room.all(:order =>'number DESC')
  	@normal_prices = Price.where(:price_type => 'normal').all
  	@vip_prices = Price.where(:price_type => 'vip').all
  	@other_prices = Price.where(:price_type => 'other').all

  end
end
