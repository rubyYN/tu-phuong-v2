class PricesController < ApplicationController

	before_filter :authorize
    before_filter :adminAuthorize

	def new
		@price = Price.new
	end
	
	def show
    	@price = Price.find(params[:id])
  	end
	def index
    	@prices = Price.all
  	end
	
	def edit
  		@price = Price.find(params[:id])
	end

	def create
	  	@price = Price.new(price_params)
		if @price.save
			flash[:notice] = "Tạo gia' thành công"
			redirect_to prices_path
		else
    		render 'new'
    	end
	end
	def update
	  @price = Price.find(params[:id])
	 
	  if @price.update(price_params)
	    redirect_to prices_path
	  else
	    render 'edit'
	  end
	end

	def destroy
		@price = Price.find(params[:id])
	  	@price.destroy
	 
	  	redirect_to prices_path
	end

	private
	  	def price_params
	   		params.require(:price).permit(:time, :price_type, :price_value)
	  	end
end
