class UsersController < ApplicationController
  
    before_filter :authorize
    before_filter :adminAuthorize
    before_filter :skip_password_attribute, only: :update

    def index
        #@users = User.joins(:role)
        @users = User.paginate(page: params[:page], per_page: 10)
    end

    def show
        @user = User.joins(:role).find(params[:id])
    end

    def new
        @user = User.new
    end

    def edit
      @user = User.find(params[:id])
    end

    def create       
        @user = User.create(user_params)

        if @user.save
            session[:user_id] = @user.id
            @user.update_attribute(:last_login_at, Time.now)
            flash[:notice] = "Tạo thành công"
            redirect_to @user
        else
            render 'new'
        end
    end

    def update
      @user = User.find(params[:id])
     
      if @user.update(user_params)
        redirect_to @user
      else
        render 'edit'
      end
    end

    def destroy
      @user = User.find(params[:id])
      @user.destroy
     
      redirect_to users_path
    end

    private
      def user_params
        params.require(:user).permit(:email, :username, :fullname, :password, :password_confirmation, :role_id)
      end

      def skip_password_attribute
        if params[:password].blank? && params[:password_confirmation].blank?
           params.except!(:password, :password_confirmation)
        end
      end
end
