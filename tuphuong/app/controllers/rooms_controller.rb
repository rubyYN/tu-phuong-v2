class RoomsController < ApplicationController

    #before_filter :authorize

	def index
    	@rooms = Room.all

    	@types = Room.uniq.pluck(:room_type)
    	@floors = Room.all.map{|f| f.number[0]}.uniq
    	@status = Room.uniq.pluck(:status)
  	end

  	def show
    	@room = Room.find(params[:id])
  	end

	def new
		@room = Room.new
	end
	
	def edit
  		@room = Room.find(params[:id])
	end

	def create
	  	@room = Room.new(room_params)
		if @room.save
			flash[:notice] = "Tạo Room thành công"
			redirect_to '/'
		else
    		render 'new'
    	end
	end

	def update
	  @room = Room.find(params[:id])

	  if params[:format] == 'json'
	  	@room.status = params[:room_status]
	  	if @room.save
	  		render :json => @room , :status => 200
	  	elsif 
	  		render :json => @room, :status => 0
  		end		
	  elsif @room.update(room_params)
	  	flash[:notice] = "Sua Room thành công"
	    redirect_to rooms_path
	  else
	    render 'edit'
	  end
	end

	def destroy
		@room = Room.find(params[:id])
	  	@room.destroy
	 
	  	redirect_to rooms_path
	end

	private
	  	def room_params
	   		params.require(:room).permit(:number, :room_type, :status)
	  	end
end
