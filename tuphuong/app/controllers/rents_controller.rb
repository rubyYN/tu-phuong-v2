class RentsController < ApplicationController
	before_filter :authorize
	def new
		@room = Room.find(params[:room_id])
		@rent = @room.rents.new
	end
	
	def show
    	@rent = Rent.find(params[:id])
  	end

	def index
    	@rents = Rent.all
  	end

  	def report
  		@users = User.all

  		@get_date_type = params[:get_date_type]
  		@get_user_id = params[:get_user_id]
  		@where = '';
  		if @get_user_id and @get_user_id.to_i > 0
  			@where =  'rents.user_id = ' + @get_user_id;
  		end
  		if @get_date_type === 'today'
  			@where = ((@where != '')? (@where + ' AND ') :'') + "DATE(rents.start_at)  = '" +  Date.today.to_s + "'"
  		elsif @get_date_type == 'month'
  			@where = ((@where != '')? (@where + ' AND ') :'') + " rents.start_at  >= '" +  Time.now.beginning_of_month.to_s + "' AND rents.start_at  <= '" +  Time.now.end_of_month.to_s + "'"
  		end
  		@reports = Rent.where(@where).order('start_at DESC').paginate(page: params[:page], per_page: 20)

    	#@reports = Rent.select('rents.*,c.*').
    	#	joins(
    	#	'LEFT JOIN rooms AS r ON r.id = rents.room_id',
    	#	'LEFT JOIN users AS u ON u.id = rents.user_id',
    	#	'LEFT JOIN clients AS c ON c.id = rents.client_id',
    	#	).where(@where).order('start_at DESC').paginate(page: params[:page], per_page: 2)

    	#respond_to do | format |  
		 # format.html # report.html.erb
		 # format.xlsx {
		 # 	render xlsx: "report", filename: "report.xlsx"  
		 # }  
		#end
  	end
	
	def edit
		@room = Room.find(params[:room_id])
  		@rent = @room.rents.find(params[:id])
	end

	def create
		@room = Room.find(params[:room_id])
		if params[:format] == 'json'
			@rent = @room.rents.new()
	  		@price = Price.where(:price_type => @room.room_type, :time => params[:rent_type]).first

			@rent.start_at = Time.now.to_datetime
			@rent.rent_type = params[:rent_type]
			@rent.user_id = current_user.id
		  	if @price
		  		@rent.price_id = @price.id
		  	end
			if @rent.save
				@room.active_rent_id = @rent.id
				@room.status = 'full'
				@room.save
				render :json => @rent , :status => 200
			else
				render :json => @rent , :status => 0
	    	end
		else
		  	@rent = @room.rents.create(rent_params)
	  		@price = Price.where(:price_type => @room.room_type, :time => params[:rent][:rent_type]).first

			@rent.start_at = Time.now.to_datetime
			@rent.user_id = current_user.id
		  	if @price
		  		@rent.price_id = @price.id
		  	end
			if @rent.save
				@room.active_rent_id = @rent.id
				@room.status = 'full'
				@room.save
				flash[:notice] = "Đặt thành công"
				# render plain: @rent.inspect
				redirect_to '/'
			else
				# render plain: params.inspect
	    		render 'new'
	    	end
	    end
	end
	def update
	  @room = Room.find(params[:room_id])
	  @rent = @room.rents.find(params[:id])
	  @price = Price.find(@rent.price_id)

	  if @rent.rent_type == '1h'
	  	@price_more = Price.where(:price_type => @room.room_type, :time => '1h+').first

	  	@hours_diff = (Time.parse(DateTime.now.to_s) - Time.parse(@rent.start_at.to_s))/3600
	  	@total_hours = (@hours_diff + 0.167).round
	  	if @total_hours > 1
	  		@rent.total_hours = @total_hours
	  		@rent.tmp_price = @price.price_value + (@total_hours - 1) * @price_more.price_value
	  	else
	  		@rent.total_hours = 1
	  		@rent.tmp_price = @price.price_value 
	  	end
	  	@rent.total_price = @rent.tmp_price
	  else
		@rent.tmp_price = @price.price_value	  		
		@rent.total_price = @price.price_value
	  end
	  @rent.end_at = Time.now.to_datetime

	  if @rent.update(rent_params)
	  	#@room.active_rent_id = 0
		@room.status = 'cleaning'
		@room.save
		flash[:notice] = "Thành công"
		if params[:format] == 'json'
			render :json => @rent , :status => 200
    	else
	    	redirect_to '/'
	    end
	  else
	    render 'edit'
	  end
	end

	def destroy
		@rent = Rent.find(params[:id])
	  	@rent.destroy
	 
	  	redirect_to '/rooms/1/rents'
	end

	private
	  	def rent_params
	   		params.require(:rent).permit(:user_id, :client_id, :room_id, :start_at, :end_at, :tmp_price, :total_price, :rent_type)
	  	end
end
