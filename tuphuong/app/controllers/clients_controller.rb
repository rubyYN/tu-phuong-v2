class ClientsController < ApplicationController

    before_filter :authorize

    def index
        #@clients = Client.all
        @clients = Client.paginate(page: params[:page], per_page: 10)
    end

    def show
        @client = Client.find(params[:id])
    end

    def new
        @client = Client.new
    end

    def edit
      @client = Client.find(params[:id])
    end

    def create       
      if params[:format] == 'json'
        @client = Client.where(:p_id => params[:client_p_id]).first
        if !@client
          @client = Client.new()
        end
        @client.name = params[:client_name]
        @client.p_id = params[:client_p_id]
        if @client.save
          if @room = Room.find(params[:room_id]) and  @room.active_rent_id and (@room.active_rent_id.to_i > 0) and @rent_for_room = @room.rents.find(@room.active_rent_id.to_i)
              @rent_for_room.client_id = @client.id
              @rent_for_room.save
          end
          render :json => @client , :status => 200
        elsif 
          render :json => @client, :status => 0
        end
      else  
        @client = Client.create(client_params)
        if @client.save
            flash[:notice] = "Tạo khách hàng thành công"
            redirect_to @client
        else
            render 'new'
        end
      end
    end

    def update
      @client = Client.find(params[:id])
     
      if @client.update(client_params)
        redirect_to @client
      else
        render 'edit'
      end
    end

    def destroy
      @client = Client.find(params[:id])
      @client.destroy
     
      redirect_to clients_path
    end

    private
      def client_params
        params.require(:client).permit(:name, :phone, :address, :p_id)
      end
end
