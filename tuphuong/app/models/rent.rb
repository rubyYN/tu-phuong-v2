class Rent < ActiveRecord::Base

  acts_as_xlsx

	belongs_to :rooms
	
    # validates :user_id, :room_id, :price_id, :client_id , :start_at, :end_at, :tmp_price, :total_price, presence: true
    #validates :rent_type , presence: true

    def vn_type
        if rent_type == '1h' 
    		  vn_type = 'Theo giờ' 
        elsif rent_type == 'day' 
         	vn_type = 'Ngày'
      	else 
        	vn_type =	'Qua đêm' 
      	end 
    end
    
    def client
      if client_id and @client = Client.find(client_id)
        client = @client
      end
    end

    def room
      if room_id and @room = Room.find(room_id)
        room = @room
      end
    end

    def user
      if user_id and @user = User.find(user_id)
        user = @user
      end
    end
end
