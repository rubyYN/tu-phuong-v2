class Role < ActiveRecord::Base
    has_many :users

        validates :name, :presence => true, 
                     :uniqueness => true,
                     format: { with: /\A[a-z]+\z/,
                                   message: "only allows lowercase letters" }

    validates :description, :presence => true

    def name_with_initial
        "#{description}"
    end
end
