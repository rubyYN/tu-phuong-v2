class Room < ActiveRecord::Base
	has_many :rents
    validates :number, :room_type, :status, presence: true

    def icon_status
    	if status == 'empty'
    		display_status = 'mdi-action-done s30'
    	elsif status == 'cleaning'
    		display_status = 'mdi-av-loop s30'
    	else
    		display_status = 'mdi-alert-warning s30'
    	end
    end
    def room_class
        if status == 'empty'
            room_class = 'card room-empty teal'
        elsif status == 'cleaning' 
            room_class = 'mix card room-cleaning orange darken-1'
        else
            room_class = 'mix card room-full red darken-1'
        end
    end
end
