class Price < ActiveRecord::Base

    validates :time, :price_type, :price_value, presence: true
    def vn_type
        if time == '1h' 
    		vn_type = 'Theo giờ' 
        elsif time == 'day' 
         	vn_type = 'Ngày'
     	elsif time == '1h+'
     		vn_type = 'Giờ tiếp theo'
      	else
          	vn_type =	'Qua đêm' 
      	end 

    end
end
